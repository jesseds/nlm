/************************************************************************
** This file is part of NLM Denoiser, a program for applying NLM filters
** on 2D/3D images.
**
** Copyright (C) 2022 Jesse Smith
**
** NLM Denoiser comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of version 3 of the GNU Lesser General Public
** License as published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with this program; if not, write to the Free Software Foundation,
** Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "nlmworker.h"
#include <QTimer>
#include <iostream>
#include <ciso646>

NlmWorker::NlmWorker(
            Tensorfff img,
            unsigned int patch,
            unsigned int search,
            float h,
            float sigma,
            bool precompute,
            std::optional<double> threshold,
            bool paginate
        ) : QObject() {

    nlm = std::make_unique<Nlm>(img);
    nlm->setPatchSize(patch);
    nlm->setSearchDist(search);
    nlm->setPrecomputePatches(precompute);
    nlm->setH(h);
    nlm->setSigma(sigma);
    nlm->setPaginate(paginate);

    if (threshold) {
        nlm->setThreshold(threshold.value());
    }

//    if (not threshold.isEmpty()) {
//        nlm->setThreshold(std::stof(threshold.toStdString()));
//    }
}

void NlmWorker::start() {
    nlm->process();
    emit finished();
}

