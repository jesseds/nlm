cmake_minimum_required(VERSION 3.15)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(MAJOR_VERSION "1")
set(MINOR_VERSION "3")
set(PATCH_VERSION "1")
set(APP_VERSION "${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION}")

project(nlm LANGUAGES CXX VERSION ${APP_VERSION})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")

find_package(Eigen3 REQUIRED)
find_package(TIFF REQUIRED)
find_package(OpenSSL COMPONENTS SSL REQUIRED)
find_package(Qt6 COMPONENTS Core Quick QuickControls2 Charts Network REQUIRED)

set(LIB_SOURCES
    inout.cpp
    nlm.cpp
)

set(LIB_INCLUDE
    inout.h
    CImg.h
    nlm.h
    )

set(GUI_SOURCES
    main.cpp
    interface.cpp
    imageprovider.cpp
    tiffreaderworker.cpp
    qml.qrc
    qtquickcontrols2.conf
    nlmworker.cpp
    updatechecker.cpp
    noisevarianceworker.cpp
    )

set(GUI_INCLUDE
    interface.h
    imageprovider.h
    tiffreaderworker.h
    nlmworker.h
    updatechecker.h
    noisevarianceworker.h
    )

set(QML_SOURCES
   main.qml
   qml/CustomButton.qml
   qml/OptionsPane.qml
   qml/HistogramChart.qml
#   qml/HistogramPanel.qml
   qml/ImageView.qml
   qml/AboutDialog.qml
   )

set(RCC_SOURCES
    qml.qrc
)

set(LIB_NAME "libnlm")

add_library(${LIB_NAME} ${LIB_SOURCES} ${LIB_INCLUDE})
set_target_properties(${LIB_NAME} PROPERTIES PREFIX "")
target_link_libraries(${LIB_NAME}
    Eigen3::Eigen
    TIFF::TIFF
)

include_directories(${TIFF_INCLUDE_DIR})

add_definitions(-DVERSION="${APP_VERSION}")
add_definitions(-DMAJOR_VERSION="${MAJOR_VERSION}")
add_definitions(-DMINOR_VERSION="${MINOR_VERSION}")
add_definitions(-DPATCH_VERSION="${PATCH_VERSION}")


get_target_property(_qmake_executable Qt6::qmake IMPORTED_LOCATION)
get_filename_component(_qt_release_dir "${_qmake_executable}" DIRECTORY)

message(${_qmake_executable})
message(${_qt_release_dir})

add_executable(${PROJECT_NAME}
    WIN32
    ${GUI_SOURCES}
    ${GUI_INCLUDE}
    ${RCC_SOURCES}
    ${QML_SOURCES}
)

target_link_libraries(${PROJECT_NAME} ${LIB_NAME})
target_link_libraries(${PROJECT_NAME}
    TIFF::TIFF
    Qt6::Core
    Qt6::Quick
    Qt6::QuickControls2
    Qt6::Charts
    Qt6::Network
)

target_compile_definitions(${PROJECT_NAME}
    PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)

install(TARGETS ${LIB_NAME} DESTINATION "bin")
install(FILES LICENSE DESTINATION "bin")

install(TARGETS ${PROJECT_NAME} DESTINATION "bin")
install(FILES LICENSE DESTINATION "bin")


if(WIN32)
    get_target_property(_qmake_executable Qt6::qmake IMPORTED_LOCATION)
    get_filename_component(_qt_bin_dir "${_qmake_executable}" DIRECTORY)
    get_filename_component(_mingw_dir "${CMAKE_CXX_COMPILER}" DIRECTORY)
    message(STATUS "ming dir: ${_mingw_dir}")

    install(FILES "${_qt_bin_dir}/libstdc++-6.dll" DESTINATION "bin")
    install(FILES "${_qt_bin_dir}/libwinpthread-1.dll" DESTINATION "bin")
    install(FILES "${_mingw_dir}/libgomp-1.dll" DESTINATION "bin")
    install(FILES "${_qt_bin_dir}/libgcc_s_seh-1.dll" DESTINATION "bin")

    if (EXISTS ${OPENSSL_ROOT_DIR})
        install(DIRECTORY "${OPENSSL_ROOT_DIR}/bin/" DESTINATION "bin" FILES_MATCHING PATTERN "*.dll")
    elseif (EXISTS ${OPENSSL_INCLUDE_DIR})
        install(DIRECTORY "${OPENSSL_INCLUDE_DIR}/../bin/" DESTINATION "bin" FILES_MATCHING PATTERN "*.dll")
    endif()

endif(WIN32)


