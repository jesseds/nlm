/************************************************************************
** This file is part of NLM Denoiser, a program for applying NLM filters
** on 2D/3D images.
**
** Copyright (C) 2022 Jesse Smith
**
** NLM Denoiser comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of version 3 of the GNU Lesser General Public
** License as published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with this program; if not, write to the Free Software Foundation,
** Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QObject>
#include <nlm.h>
#include <QImage>
#include <nlmworker.h>
#include <tiffreaderworker.h>
#include <QTimer>
#include <QFileInfo>
#include <unsupported/Eigen/CXX11/Tensor>
#include <QSettings>
#include <noisevarianceworker.h>


typedef Eigen::Tensor<float, 3> Tensorfff;


class Interface : public QObject {
    Q_OBJECT

    // QML-interactable properties
    Q_PROPERTY(QString tiff READ tiff WRITE setTiff)
    Q_PROPERTY(QString autosaveTiffPath READ autosaveTiffPath WRITE setAutosaveTiffPath NOTIFY autosaveTiffPathChanged)
    Q_PROPERTY(bool autosaveEnabled READ autosaveEnabled WRITE setAutosaveEnabled NOTIFY autosaveEnabledChanged)
    Q_PROPERTY(unsigned int patchSize READ patchSize WRITE setPatchSize NOTIFY patchSizeChanged)
    Q_PROPERTY(unsigned int searchDist READ searchDist WRITE setSearchDist NOTIFY searchDistChanged)
    Q_PROPERTY(double h READ h WRITE setH NOTIFY hChanged)
    Q_PROPERTY(double sigma READ sigma WRITE setSigma NOTIFY sigmaChanged)
    Q_PROPERTY(bool precomputePatches READ precomputePatches WRITE setPrecomputePatches NOTIFY precomputePatchesChanged)
    Q_PROPERTY(bool paginate READ paginate WRITE setPaginate NOTIFY paginateChanged)
    Q_PROPERTY(QString thresholdStr READ thresholdStr WRITE setThresholdStr NOTIFY thresholdChanged)
    Q_PROPERTY(unsigned int nPages READ nPages NOTIFY nPagesChanged)
    Q_PROPERTY(unsigned int currPage READ currPage WRITE setCurrPage NOTIFY currPageChanged)
    Q_PROPERTY(bool resultAvailable READ resultAvailable NOTIFY resultAvailableChanged)
    Q_PROPERTY(QPointF unfilteredHistVal READ unfilteredHistVal NOTIFY unfilteredHistValChanged)
    Q_PROPERTY(QPointF filteredHistVal READ filteredHistVal NOTIFY filteredHistValChanged)

    Q_PROPERTY(double elapsedSecs READ elapsedSecs)
    Q_PROPERTY(long elapsedMins READ elapsedMins)
    Q_PROPERTY(long elapsedHours READ elapsedHours)

private:
    // Internal QML property variables
    QString _tiff_path {};
    QString _autosave_tiff_path {};
    bool _autosave_enabled = false;
    unsigned int _patch_size = 5;
    unsigned int _search_dist = 11;
    double _h = 0.1;
    double _sigma = 0;
    bool _precompute_patches = false;
    bool _paginate = false;
    std::optional<double> _threshold = std::nullopt;
    unsigned int _npages = 1;
    unsigned int _curr_page = 1;
    bool _result_available = false;
    QPointF _unfilteredHistVal;
    QPointF _filteredHistVal;
    inout::TiffScalar vtk_tif_type = inout::TiffScalar::FLOAT;
    QSettings _settings;

    // Other internal variables
    QImage _curr_unfiltered_image{};
    QImage _curr_filtered_image{};
    NlmWorker* _nlm_worker;
    TiffReaderWorker* _tiff_worker;
    NoiseVarianceWorker* _noise_var_worker;

    Tensorfff _result;
    QThread _worker_thread;
    QThread _read_tiff_thread;
    QThread _noise_var_thread;
    bool _unsaved = false;
    unsigned int _nbins = 128;
    QTimer* _prog_timer;
    long _elapsed_hours = 0;
    double _elapsed_secs = 0;
    long _elapsed_mins = 0;

    // Internal functions
    QImage tensor2qimage(Tensorfff const& ten, unsigned int page=1);
    void processResult();
    void workerProgressChanged();
    void nlmWorkerDestroyed();
    void noiseVarWorkerFinished();
    void loadTiffCompleted();
    void buildFilteredHistogram();
    std::pair<std::vector<float>, std::vector<float>> histogram(Tensorfff const& tensor, size_t nbins);

public:
    explicit Interface();
    ~Interface();

    Tensorfff _image;
    Q_INVOKABLE QString patchCacheMemoryRequiredStr();
    Q_INVOKABLE void recomputeImages();
    Q_INVOKABLE void saveTiff(QString const& url);
    Q_INVOKABLE bool isUnsaved() const { return _unsaved; }
    Q_INVOKABLE QString tifName() const {return QFileInfo(_tiff_path).fileName();}
    Q_INVOKABLE QString autosaveTifName() const {return QFileInfo(_autosave_tiff_path).fileName();}
    Q_INVOKABLE void processStart();
    Q_INVOKABLE void processCancel();
    Q_INVOKABLE void estimateNoiseVariance();

    // The patchsize for pixel similarity comparison
    auto patchSize() const {return _patch_size;}
    void setPatchSize(unsigned int val);

    // Paginate 3D images
    auto paginate() const {return _paginate;}
    void setPaginate(bool val);

    // The radius of the neighborhood to search for similar pixels
    auto searchDist() const {return _search_dist;}
    void setSearchDist(unsigned int val);

    // Smoothing factor that affects how similiar dissimilar patches are categorized
    auto h() const {return _h;}
    void setH(double val);

    // Estimate of the (Gaussian) noise standard deviation in the image
    auto sigma() const {return _sigma;}
    void setSigma(double val);

    // Only pixels with scalars higher than a (optional) threshold will be filtered
    auto threshold() const {return _threshold;}
    QString thresholdStr() const;
    void setThresholdStr(QString val);

    // Whether or not to compute and store all patches before NLM starts
    auto precomputePatches() const {return _precompute_patches;}
    void setPrecomputePatches(bool val);

    // Number of pages, or z-dimension of the image
    auto nPages() const {return _npages;}

    // Current page shown in the image viewers
    auto currPage() const {return _curr_page;}
    void setCurrPage(unsigned int val);

    // Whether a valid nlm result is available
    auto resultAvailable() const {return _result_available;}
    void setResultAvailable(bool val);

    // Get the current preview-able image from original tiff
    QImage currUnfilteredImage() const {return _curr_unfiltered_image;};
    QImage currFilteredImage() const {return _curr_filtered_image;}

    // For populating the unfiltered image histogram
    auto unfilteredHistVal() const {return _unfilteredHistVal;}

    // For populating the filtered image histogram
    auto filteredHistVal() const {return _filteredHistVal;}

    // Set the NLM tiff
    void setTiff(QString const& path);
    QString const& tiff() const {return _tiff_path;}

    // Autosave tiff path
    void setAutosaveTiffPath(QString const& path);
    auto const& autosaveTiffPath() const {return _autosave_tiff_path;}

    // Autosave enabled
    void setAutosaveEnabled(bool val);
    auto autosaveEnabled() const  {return _autosave_enabled;}

    // Elapsed times
//    long elapsedMSecs() const {return _elapsed_msecs;}
    double elapsedSecs() const {return _elapsed_secs;}
    long elapsedMins() const {return _elapsed_mins;}
    long elapsedHours() const {return _elapsed_hours;}

signals:
    void tiffLoadingStarted();
    void tiffLoadingEnded(bool);
    void showWorkingProgress();
    void endWorkingProgress();
    void autosaveTiffPathChanged(QString);
    void autosaveEnabledChanged(bool);
    void patchSizeChanged(unsigned int);
    void paginateChanged(bool);
    void searchDistChanged(unsigned int);
    void unfilteredImageChanged(QImage);
    void filteredImageChanged(QImage);
    void hChanged(double);
    void sigmaChanged(double);
    void precomputePatchesChanged(bool);
    void thresholdChanged(QString);
    void nPagesChanged(unsigned int);
    void currPageChanged(unsigned int);
    void resultAvailableChanged(bool);  // value is whether the result is valid or not
    void unfilteredHistValChanged(QPointF);
    void filteredHistValChanged(QPointF);
    void showMessage(QString title, QString msg);
    void filteredHistogramComputed();
    void unfilteredHistogramComputed();

    void processStarted();
    void processFinished(bool);  // value is whether the NLM filter was successful or not (i.e. aborted)
    void processProgressChanged(float);
    void nlmWorkerObjDestroyed();
};
