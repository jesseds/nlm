/************************************************************************
** This file is part of NLM Denoiser, a program for applying NLM filters
** on 2D/3D images.
**
** Copyright (C) 2022 Jesse Smith
**
** NLM Denoiser comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of version 3 of the GNU Lesser General Public
** License as published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with this program; if not, write to the Free Software Foundation,
** Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <imageprovider.h>
#include <iostream>
#include <QRgb>

ImageProvider::ImageProvider(Interface* interf) : QQuickImageProvider(QQuickImageProvider::Image) {
    interface_ = interf;
}

QImage ImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize) {
    auto parts = id.split("/");
    auto dims = interface_->_image.dimensions();

    if (parts.at(0) == "unfiltered") {
        int width = interface_->currUnfilteredImage().width();
        int height = interface_->currUnfilteredImage().height();

        if (size)
            *size = QSize(width, height);

        return interface_->currUnfilteredImage();
    }
    else {
        int width = interface_->currFilteredImage().width();
        int height = interface_->currFilteredImage().height();

        if (size)
            *size = QSize(width, height);

        return interface_->currFilteredImage();

    }
}
