/************************************************************************
** This file is part of NLM Denoiser, a program for applying NLM filters
** on 2D/3D images.
**
** Copyright (C) 2022 Jesse Smith
**
** NLM Denoiser comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of version 3 of the GNU Lesser General Public
** License as published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with this program; if not, write to the Free Software Foundation,
** Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <interface.h>
#include <inout.h>
#include <iostream>
#include <QFile>
#include <QFileInfo>
#include <QUrl>
#include <QImage>
#include <ciso646>
#include <QDir>
#include <math.h>


Interface::Interface() : QObject() {
    setSigma(_settings.value("sigma", 0).toDouble());
    setH(_settings.value("h", 0).toDouble());
    setPatchSize(_settings.value("patch_size", 5).toInt());
    setSearchDist(_settings.value("search_dist", 5).toInt());
    setThresholdStr(_settings.value("threshold", "").toString());
    setPaginate(_settings.value("paginate").toBool());
}


std::pair<std::vector<float>, std::vector<float>> Interface::histogram(Tensorfff const& tensor, size_t nbins) {
    float min = ((Eigen::Tensor<float, 0>)tensor.minimum())(0);
    float max = ((Eigen::Tensor<float, 0>)tensor.maximum())(0);
    float bin_width = (max - min)/nbins;

    std::vector<float> edges{0};
    std::vector<float> counts{0};
    std::vector<float> ecounts{0};
    counts.resize(nbins);

    for (size_t i=0; i<nbins; ++i) {
        edges.push_back(min + i*bin_width);
        edges.push_back(min + (i + 1)*bin_width);
    }

    long idx = 0;
    float val = 0;
    for (long i=0; i<tensor.size(); ++i) {
        val = *(tensor.data() + i);
        idx = (val - min)/(bin_width);

        idx = (idx < nbins) ? idx : nbins - 1;
        counts.at(idx) += 1;
    }

    for (auto i: counts) {
        ecounts.push_back(i);
        ecounts.push_back(i);
    }

    assert(ecounts.size() == edges.size());
    return {edges, ecounts};
}


Interface::~Interface() {
    _worker_thread.quit();
    _worker_thread.wait();
    _read_tiff_thread.quit();
    _read_tiff_thread.wait();
    _noise_var_thread.quit();
    _noise_var_thread.wait();

}


QImage Interface::tensor2qimage(Tensorfff const& tensor, unsigned int page) {
    long page_idx = page-1;
    auto dims = tensor.dimensions();
    QImage retn = QImage(dims[0], dims[1], QImage::Format_Grayscale8);
    float currval;
    unsigned int newval;
    float transval;

    float min = ((Eigen::Tensor<float, 0>)tensor.minimum())(0);
    float max = ((Eigen::Tensor<float, 0>)tensor.maximum())(0);

    for (long j=0; j<dims[1]; ++j) {
        for (long i=0; i<dims[0]; ++i) {
            currval = tensor(i, j, page_idx);
            transval = (currval - min) * (255/(max - min));
            newval = static_cast<unsigned int>(transval);
            retn.setPixelColor(i, j, QColor::fromRgb(newval, newval, newval));
        }
    }

    return retn;
}


void Interface::recomputeImages() {
    auto dims = _image.dimensions();
    auto count = dims[0]*dims[1]*dims[2];

    if (count > 1) {
        _curr_unfiltered_image = tensor2qimage(_image, _curr_page);
        emit unfilteredImageChanged(_curr_unfiltered_image);
    }

    if (_result_available) {
        _curr_filtered_image = tensor2qimage(_result, _curr_page);
        emit filteredImageChanged(_curr_filtered_image);
    }
}


void Interface::setCurrPage(unsigned int val) {
    if (val > _npages or val < 1)
        return;

    if (val != _curr_page) {
        _curr_page = val;
        emit currPageChanged(val);
        recomputeImages();
    }
}


void Interface::setResultAvailable(bool val) {
    if (val != _result_available) {
        _result_available = val;
        emit resultAvailableChanged(val);
    }
}


void Interface::setTiff(QString const& path) {
    emit tiffLoadingStarted();
    QUrl url(path);
    auto file_path = url.toLocalFile();
    QFileInfo info(file_path);

    if ((not info.exists()) or (file_path == "")) {
        std::cerr << "File does not exist" << std::endl;
        emit tiffLoadingEnded(false);
        return;
    }
    _tiff_path = file_path;

    _tiff_worker = new TiffReaderWorker(file_path);
    _tiff_worker->moveToThread(&_read_tiff_thread);
    connect(&_read_tiff_thread, &QThread::finished, _tiff_worker, &TiffReaderWorker::deleteLater);
    connect(&_read_tiff_thread, &QThread::started, _tiff_worker, &TiffReaderWorker::start);
    connect(_tiff_worker, &TiffReaderWorker::finished, this, &Interface::loadTiffCompleted);

    _read_tiff_thread.start();
}


void Interface::setAutosaveTiffPath(const QString& path) {
    if (path != _autosave_tiff_path) {
        _autosave_tiff_path = path;
        emit autosaveTiffPathChanged(path);
    }
}


void Interface::setAutosaveEnabled(bool val) {
    if (val != _autosave_enabled) {
        _autosave_enabled = val;
        emit autosaveEnabledChanged(val);
    }
}


void Interface::loadTiffCompleted() {
    if (_tiff_worker->result.has_value()) {
        _image = std::move(_tiff_worker->result.value());
        vtk_tif_type = _tiff_worker->tif_vtk_type;

        auto dims = _image.dimensions();
        _npages = dims[2];

        setResultAvailable(false);
        setCurrPage(1);
        recomputeImages();

        auto [centers, counts] = histogram(_image, _nbins);

        emit nPagesChanged(_npages);
        emit tiffLoadingEnded(true);

        QPointF point;
        for (size_t i=0; i< centers.size(); ++i) {
            point.setX(centers.at(i));
            point.setY(counts.at(i));
            emit unfilteredHistValChanged(point);
        }
        emit unfilteredHistogramComputed();
    }
    else {
        emit showMessage("Failed to read tiff", QString::fromStdString(_tiff_worker->err_msg));
        emit tiffLoadingEnded(false);
    }

    _read_tiff_thread.quit();
    _read_tiff_thread.wait();
}


void Interface::setPatchSize(unsigned int val) {
    if (val % 2 == 0) {
        val += 1;
    }
    if (val != _patch_size) {
        _patch_size = val;
        emit patchSizeChanged(val);
        _settings.setValue("patch_size", val);
    }
}


void Interface::setPaginate(bool val) {
    if (val == _paginate) {
        return;
    }

    _paginate = val;
    emit paginateChanged(val);
    _settings.setValue("paginate", val);
}


void Interface::setSearchDist(unsigned int val) {
    if (val != _search_dist) {
        _search_dist = val;
        emit searchDistChanged(val);
        _settings.setValue("search_dist", val);
    }
}


void Interface::setH(double val) {
    std::cout << "New h: " << val << "\n";
    if (not isClose(val, _h)) {
        _h = val;
        emit hChanged(val);
        _settings.setValue("h", val);
    }
}


void Interface::setSigma(double val) {
    if (not isClose(val, _sigma)) {
        _sigma = val;
        emit sigmaChanged(val);
        _settings.setValue("sigma", val);
    }
}


void Interface::setPrecomputePatches(bool val) {
    _precompute_patches = val;
    emit precomputePatchesChanged(val);
}


QString Interface::thresholdStr() const {
    if (_threshold) {
        return QString::number(_threshold.value());
    }
    else {
        return QString("");
    }
}


void Interface::setThresholdStr(QString val) {
    if (_threshold and val.isEmpty()) {
        _threshold = std::nullopt;
        emit thresholdChanged(val);
        _settings.setValue("threshold", val);
        return;
    }


    if (not val.isEmpty()) {
        double val_double = std::stod(val.toStdString());

        if (not _threshold) {
            _threshold = val_double;
            emit thresholdChanged(val);
            _settings.setValue("threshold", val);
        }
        else {
            if (not isClose(_threshold.value(), val_double)) {
                _threshold = val_double;
                emit thresholdChanged(val);
                _settings.setValue("threshold", val);
            }
        }
    }
}


Q_INVOKABLE void Interface::saveTiff(QString const& url) {
    QFileInfo path = QFileInfo(QUrl(url).toLocalFile());

    if (not path.dir().exists()) {
        emit showMessage("Failed to save tif file", "Specified tiff location does not exist.");
        return;
    }
    inout::write_tiff(path.absoluteFilePath().toStdString(), _result, vtk_tif_type);
    _unsaved = false;
}


Q_INVOKABLE QString Interface::patchCacheMemoryRequiredStr() {
    double memory = patchCacheMemoryRequired(_image, patchSize(), paginate());

    std::stringstream retn {};
    retn << std::fixed << std::setprecision(3) << memory;
    return QString::fromStdString(retn.str());
}

Q_INVOKABLE void Interface::processStart() {
    if (_tiff_path == "") {
        return;
    }

    _prog_timer = new QTimer(this);
    _prog_timer->setInterval(100);
    connect(_prog_timer, &QTimer::timeout, this, &Interface::workerProgressChanged);

    _nlm_worker = new NlmWorker(
                _image,
                patchSize(),
                searchDist(),
                h(),
                sigma(),
                precomputePatches(),
                threshold(),
                paginate()
                );

    emit processStarted();

    _nlm_worker->moveToThread(&_worker_thread);
    connect(&_worker_thread, &QThread::finished, _nlm_worker, &NlmWorker::deleteLater);
    connect(&_worker_thread, &QThread::started, _nlm_worker, &NlmWorker::start);
    connect(_nlm_worker, &NlmWorker::finished, this, &Interface::processResult);
    connect(_nlm_worker, &NlmWorker::finished, _prog_timer, &QTimer::stop);
    connect(_nlm_worker, &NlmWorker::destroyed, this, &Interface::nlmWorkerDestroyed);

    _worker_thread.start();
    _prog_timer->start();
}


void Interface::workerProgressChanged() {
    auto prog = _nlm_worker->nlm->progress();
    emit processProgressChanged(prog);
}

void Interface::nlmWorkerDestroyed() {
    emit nlmWorkerObjDestroyed();
}

void Interface::processCancel() {
    _nlm_worker->nlm->abort = true;
}


void Interface::estimateNoiseVariance() {

    _noise_var_worker = new NoiseVarianceWorker(_image, _threshold);
    _noise_var_worker->moveToThread(&_noise_var_thread);
    connect(&_noise_var_thread, &QThread::finished, _noise_var_worker, &NoiseVarianceWorker::deleteLater);
    connect(&_noise_var_thread, &QThread::started, _noise_var_worker, &NoiseVarianceWorker::start);

    connect(_noise_var_worker, &NoiseVarianceWorker::finished, this, &Interface::noiseVarWorkerFinished);

    emit showWorkingProgress();
    _noise_var_thread.start();

}


void Interface::noiseVarWorkerFinished() {
    setSigma(std::sqrt(_noise_var_worker->result));
    emit endWorkingProgress();
    _noise_var_thread.quit();
    _noise_var_thread.wait();
}


void Interface::processResult() {
    if (_nlm_worker->nlm->abort == false) {
        _unsaved = true;
        _result = std::move(_nlm_worker->nlm->_result);
        _elapsed_secs = _nlm_worker->nlm->elapsedSecs();
        _elapsed_hours = _nlm_worker->nlm->elapsedHours();
        _elapsed_mins = _nlm_worker->nlm->elapsedMins();
        setResultAvailable(true);
        recomputeImages();
        emit processFinished(true);
        buildFilteredHistogram();

        if (autosaveEnabled() == true and autosaveTiffPath() != "") {
            saveTiff(_autosave_tiff_path);
        }
        setAutosaveEnabled(false);

    }
    else {
        emit processFinished(false);
    }
    _worker_thread.quit();
    _worker_thread.wait();

}


void Interface::buildFilteredHistogram() {
    if (_nlm_worker->nlm->abort == false) {
        auto [edges, counts] = histogram(_result, _nbins);
        QPointF point;
        for (size_t i=0; i< edges.size(); ++i) {
            point.setX(edges.at(i));
            point.setY(counts.at(i));
            emit filteredHistValChanged(point);
        }
    }
    emit filteredHistogramComputed();
}
