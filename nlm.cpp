﻿/************************************************************************
** This file is part of NLM Denoiser, a program for applying NLM filters
** on 2D/3D images.
**
** Copyright (C) 2022 Jesse Smith
**
** NLM Denoiser comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of version 3 of the GNU Lesser General Public
** License as published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with this program; if not, write to the Free Software Foundation,
** Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <nlm.h>
#include <inout.h>
#include <math.h>
#include <sstream>
#include <ciso646>
#include <algorithm>
#include <execution>

#undef emit

bool isClose(double val1, double val2, double dx) {
    if (std::abs(val2 - val1) < dx) {
        return true;
    }
    else {
        return false;
    }
}


double mad_estimate_noise_variance(Tensorfff const& img, std::optional<double> threshold) {
    if (img.size() <= 1) {
        return 0;
    }

    auto dims = img.dimensions();

    if (dims[0] * dims[1] * dims[2] == 1) {
        return 0;
    }

    int kern_size = 1;
    kern_size *= dims[0] == 1 ? 1 : 3;
    kern_size *= dims[1] == 1 ? 1 : 3;
    kern_size *= dims[2] == 1 ? 1 : 3;

    int nthreads = omp_get_num_procs();

    std::vector<std::vector<float>> diffs (nthreads);

    for (auto & vec: diffs) {
        vec.reserve(img.size() / nthreads + 1);
    }

    #pragma omp parallel for collapse(3) num_threads(nthreads)
    for (long k=0; k<dims[2]; ++k) {
        for (long j=0; j<dims[1]; ++j) {
            for (long i=0; i<dims[0]; ++i) {
                int thread = omp_get_thread_num();
                std::vector<float> x (kern_size);

                float val1 = 0;
                float val2 = 0;
                float newval = 0;
                int I = 0;
                int K = 0;
                int J = 0;
                int N = 0;

                if (threshold) {
                    if (img(i, j, k) < threshold.value()) {
                        continue;
                    }
                }

                // The kernel
                for (long kk=k-1; kk<=k+1; ++kk) {
                    // Skip non-zero indices if the image is not 3D in this direction
                    if (dims[2] == 1 and kk != 0) {
                        continue;
                    }

                    // k boundary mirroring
                    if (kk == -1) {
                        K = k+1;
                    }
                    else if (kk == dims[2]) {
                        K = dims[2] - 2;
                    }
                    else {
                        K = kk;
                    }

                    for (long kj=j-1; kj<=j+1; ++kj) {
                        // Skip non-zero indices if the image is not 3D in this direction
                        if (dims[1] == 1 and kj != 0) {
                            continue;
                        }

                        // j boundary mirroring
                        if (kj == -1) {
                            J = j+1;
                        }
                        else if (kj == dims[1]) {
                            J = dims[1] - 2;
                        }
                        else {
                            J = kj;
                        }

                        for (long ki=i-1; ki<=i+1; ++ki) {
                            // Skip non-zero indices if the image is not 3D in this direction
                            if (dims[0] == 1 and ki != 0) {
                                continue;
                            }

                            // i boundary mirroring
                            if (ki == -1) {
                                I = i+1;
                            }
                            else if (ki == dims[0]) {
                                I = dims[0] - 2;
                            }
                            else {
                                I = ki;
                            }

                            x[N] = img(I, J, K);
                            ++N;
                        }
                    }
                }
                std::sort(x.begin(), x.end());

                // Odd median
                if (x.size() % 2 == 1) {
                    newval = x[(x.size() - 1)/2];
                }
                // Even median
                else {
                    val1 = x[x.size()/2];
                    val2 = x[x.size()/2 + 1];
                    newval = (val1 + val2) / 2;
                }
                float diff = std::abs(img(i, j, k) - newval);
                diffs[thread].push_back(diff);
            }
        }
    }

    std::vector<float> diff {};

    for (int i=0; i<diffs.size(); ++i) {
        std::move(diffs.at(i).begin(), diffs.at(i).end(), std::back_inserter(diff));
    }

    if (diff.size() == 0) {
        return 0;
    }

    long size = diff.size();
    std::sort(std::execution::par, diff.data(), diff.data() + size);

    double retn = 0;
    double temp1 = 0;
    double temp2 = 0;

    // Odd median
    if (size % 2 == 1) {
        retn = diff[(size - 1)/2];
    }
    // Even median
    else {
        temp1 = diff[size/2];
        temp2 = diff[size/2 + 1];
        retn = (temp1 + temp2)/2;
    }

    return retn*retn;
}


Nlm::Nlm(Tensorfff const& img) {
    setImage(img);
    _setup();
}


Nlm::Nlm(std::string const& path) {
    auto data = inout::read_tiff(path);
    setImage(std::move(data.first));
    tif_vtk_type = data.second;
    _setup();
}


void Nlm::_setup() {
    setPatchSize(5);
    setSearchDist(11);
    setSigma(0);
    setH(0);
    setPrecomputePatches(false);
    setPaginate(false);
    _patch_cache_2d = std::make_unique<std::vector<Eigen::ArrayXXf>>();
    _patch_cache_3d = std::make_unique<std::vector<Tensorfff>>();

    auto dims = _img.dimensions();
    _result = Tensorfff(dims[0], dims[1], dims[2]);
    _result.setZero();
}


void Nlm::setPatchSize(unsigned int val) {
    if (val % 2 == 0 or val < 3) {
        throw std::invalid_argument("Patch size must be odd and >= 3");
    }
    else {
        _patch_size = val;
        _patch_rad = (_patch_size - 1)/2;

        _patch_offset = std::vector<int>(_patch_size);

        for (size_t i=0; i<_patch_size; ++i) {
            _patch_offset[i] = i - patchDist();
        }
    }
}


void Nlm::setPaginate(bool val) {
    if (_paginate == val) {
        return;
    }

    _paginate = val;
}


void Nlm::setH(float val) {
    if (val < 0) {
        throw std::invalid_argument("h value must be >= 0");
    }
    else {
        _h = val;
    }
}


void Nlm::setSearchDist(unsigned int val) {
    if (val < 1) {
        throw std::invalid_argument("Search distance must be at least 1");
    }
    else {
        _search_dist = val;
    }
}


void Nlm::_rebuild_patch_cache() {
    auto dims = _img.dimensions();
    auto count = dims[0]*dims[1]*dims[2];

    if (dims[2] == 1) {
        _patch_cache_2d = std::make_unique<std::vector<Eigen::ArrayXXf>>(count);

        std::for_each(std::execution::par, _dim0_loop.cbegin(), _dim0_loop.cend(), [&](int i) {
            size_t idx = 0;
            for (int j=0; j<dims[1]; ++j) {
                idx = j*dims[0] +  i;
                _patch_cache_2d->at(idx) = _patch_2d(i, j, dims[0], dims[1]);
            }
        });
    }
    else {
        _patch_cache_3d = std::make_unique<std::vector<Tensorfff>>(count);

        std::for_each(std::execution::par, _dim0_loop.cbegin(), _dim0_loop.cend(), [&](int i) {
            size_t idx = 0;
            for (int j=0; j<dims[1]; ++j) {
                for (int k=0; k<dims[2]; ++k) {
                if (abort == true) {
                    continue;
                }
                    idx = i*dims[2]*dims[1] + j*dims[2] + k;
                    _patch_cache_3d->at(idx) = _patch_3d(i, j, k, dims[0], dims[1], dims[2]);
                }
            }
        });
    }
}


Eigen::ArrayXXf Nlm::_patch_2d(int p0, int p1, int dim0, int dim1)  const {
    int patch_i = 0;
    int patch_j = 0;
    Eigen::ArrayXXf patch(patchSize(), patchSize());

    int rad = (_patch_size - 1)/2;

    for (int i=-rad; i<=rad; ++i) {
        patch_i = i + p0;
        // Reflect the patch index if it lies outside the image (padding).
        if (patch_i < 0) {
            patch_i += -patch_i*2;
        }
        if (patch_i >= dim0) {
            patch_i += (dim0 - patch_i - 1)*2;
        }

        for (int j=-rad; j<=rad; ++j) {
            patch_j = j + p1;
            // Reflect the patch ind if it lies outside the image (padding).
            if (patch_j < 0) {
                patch_j += -patch_j*2;
            }
            if (patch_j >= dim1) {
                patch_j += (dim1 - patch_j - 1)*2;
            }

            patch(i + rad, j + rad) = _img(patch_i, patch_j, 0);
        }
    }
    return patch;
}


Tensorfff Nlm::_patch_3d(int p0, int p1, int p2, int dim0, int dim1, int dim2)  const {
    int rad = _patch_rad;

    int patch_i = 0;
    int patch_j = 0;
    int patch_k = 0;
    Tensorfff patch(_patch_size, _patch_size, _patch_size);

    for (int k=-rad; k<=rad; ++k) {
        patch_k = k + p2;
        // Reflect the patch index if it lies outside the image (padding).
        if (patch_k < 0) {
            patch_k += -patch_k*2;
        }
        if (patch_k >= dim2) {
            patch_k += (dim2 - patch_k - 1)*2;
        }

        for (int j=-rad; j<=rad; ++j) {
            patch_j = j + p1;
            // Reflect the patch index if it lies outside the image (padding).
            if (patch_j < 0) {
                patch_j += -patch_j*2;
            }
            if (patch_j >= dim1) {
                patch_j += (dim1 - patch_j - 1)*2;
            }

            for (int i=-rad; i<=rad; ++i) {
                patch_i = i + p0;
                // Reflect the patch index if it lies outside the image (padding).
                if (patch_i < 0) {
                    patch_i += -patch_i*2;
                }
                if (patch_i >= dim0) {
                    patch_i += (dim0 - patch_i - 1)*2;
                }

                patch(i + rad, j + rad, k + rad) = _img(patch_i, patch_j, patch_k);
            }
        }
    }

    return patch;
}


void Nlm::setSigma(double val) {
    if (val < 0) {
        throw std::invalid_argument("Sigma cannot be negative");
    }
    else {
        _sigma = val;
    }
}


void Nlm::_nlm_2d() {
    auto dims = _img.dimensions();
    _result = Tensorfff(dims[0], dims[1], 1);
    _result.setZero();

    int ssize = searchSize();
    float A = (static_cast<float>(patchSize()) - 1)/2;
    auto var2 = 2*(sigma()*sigma());
    int psize2 = _patch_size * _patch_size;

    Eigen::ArrayXXf spat_weight(patchSize(), patchSize());

    // Construct kernel for spatial weighting
    for (auto i: _patch_offset) {
        for (auto j: _patch_offset) {
            spat_weight(i + patchDist(), j + patchDist()) = expf( -(i*i + j*j)/(2*A*A) );
        }
    }
    spat_weight /= (spat_weight.sum() * _h * _h);

    #pragma omp parallel for collapse(2) schedule(dynamic)
    for (int p1=0; p1<dims[1]; ++p1) {
        for (int p0=0; p0<dims[0]; ++p0) {
            int tid = omp_get_thread_num();
            Eigen::ArrayXXf diff{_patch_size, _patch_size};
            Eigen::ArrayXXf patch0(_patch_size, _patch_size);
            Eigen::ArrayXXf patch1{_patch_size, _patch_size};
            float weight_sum = 0;
            float diff_sum = 0;
            float new_val = 0;
            float weight = 0;
            float val = 0;
            weight_sum = 0;
            new_val = 0;

            if (abort == true) {
                continue;
            }

            // Keep original value if _threshold is set and original value is larger than _threshold
            if (_threshold) {
                if (_img(p0, p1, 0) < _threshold.value()) {
                    _result(p0, p1, 0) = _img(p0, p1, 0);
                    if (tid == 0) {
                    _completed_iterations += omp_get_num_threads();
                    }
                    continue;
                }
            }
            if (precomputePatches() == true)
                patch0 = _patch_cache_2d->at(dims[0]*p1 + p0);
            else
                patch0 = _patch_2d(p0, p1, dims[0], dims[1]);

            for (int q0=p0-ssize/2; q0<=p0+ssize/2; ++q0) {
                if (q0 < 0 or q0 >= dims[0]) {
                    continue;
                }

                for (int q1=p1-ssize/2; q1<=p1+ssize/2; ++q1) {
                    if (q1 < 0 or q1 >= dims[1]) {
                        continue;
                    }

                    if (_precompute_patches == true)
                        patch1 = _patch_cache_2d->at(dims[0]*q1 + q0);
                    else
                        patch1 = _patch_2d(q0, q1, dims[0], dims[1]);

                    diff = patch1 - patch0;

                    diff_sum = 0;
                    for (size_t i=0; i<psize2; ++i) {
                        val = spat_weight.data()[i] * (diff.data()[i]*diff.data()[i] - var2);
                        if (val > 0)
                            diff_sum += val;
                    }

                    weight = expf(-diff_sum);
                    weight_sum += weight;
                    new_val += weight*_img(q0, q1, 0);
                }
            }
            _result(p0, p1, 0) = new_val/weight_sum;
            if (tid == 0) {
                _completed_iterations += omp_get_num_threads();
            }
        }
    }
}


double Nlm::progress() const {
    auto dims = _img.dimensions();

    int count = 0;
    if (_paginate and dims[2] > 1) {
        count = dims[2];
    }
    else {
        count = dims[2]*dims[1]*dims[0];;
    }

    double prog = _completed_iterations/static_cast<double>(count);
    return prog > 1 ? 1 : prog;
}


void Nlm::_nlm_3d() {
    auto dims = _img.dimensions();
    _result = Tensorfff(dims[0], dims[1], dims[2]);
    _result.setZero();

    int ssize = searchSize();
    float A = (static_cast<float>(patchSize()) - 1)/4;
    auto var2 = 2*(_sigma * _sigma);
    int psize2 = _patch_size * _patch_size * _patch_size;

    Tensorfff spat_weight(patchSize(), patchSize(), patchSize());

    // Construct kernel for spatial weighting
    for (auto i: _patch_offset) {
        for (auto j: _patch_offset) {
            for (auto k: _patch_offset) {
                spat_weight(i + patchDist(), j + patchDist(), k + patchDist()) = expf( -(i*i + j*j + k*k)/(2*A*A) );
            }
        }
    }
    Eigen::Tensor<float, 0> spat_weight_sum = spat_weight.sum();
    spat_weight /= spat_weight.constant(spat_weight_sum(0) * _h * _h);

    //    #pragma omp target teams distribute parallel for
    #pragma omp parallel for collapse(3) schedule(dynamic)
    for (int p1=0; p1<dims[1]; ++p1) {
        for (int p0=0; p0<dims[0]; ++p0) {
            for (int p2=0; p2<dims[2]; ++p2) {
                auto tid = omp_get_thread_num();
                Tensorfff diff{patchSize(), patchSize(), patchSize()};
                Tensorfff patch0(patchSize(), patchSize(), patchSize());
                Tensorfff patch1{patchSize(), patchSize(), patchSize()};
                float weight_sum = 0;
                float diff_sum;
                float new_val = 0;
                float weight = 0;
                float val = 0;

                if (abort == true) {
                    continue;
                }

                weight_sum = 0;
                new_val = 0;

                // Keep original value if _threshold is set and original value is larger than _threshold
                if (_threshold) {
                    if (_img(p0, p1, p2) < _threshold.value()) {
                        _result(p0, p1, p2) = _img(p0, p1, p2);
                        if (tid == 0) {
                            _completed_iterations += omp_get_num_threads();
                        }
                        continue;
                    }
                }

                if (_precompute_patches == true)
                    patch0 = _patch_cache_3d->at(dims[1]*dims[2]*p0 + dims[2]*p1 + p2);
                else
                    patch0 = _patch_3d(p0, p1, p2, dims[0], dims[1], dims[2]);

                for (int q0=p0-ssize/2; q0<=p0+ssize/2; ++q0) {
                    if (q0 < 0 or q0 >= dims[0]) {
                        continue;
                    }

                    for (int q1=p1-ssize/2; q1<=p1+ssize/2; ++q1) {
                        if (q1 < 0 or q1 >= dims[1]) {
                            continue;
                        }

                        for (int q2=p2-ssize/2; q2<=p2+ssize/2; ++q2) {
                            if (q2 < 0 or q2 >= dims[2])
                                continue;

                            if (_precompute_patches == true)
                                patch1 = _patch_cache_3d->data()[dims[1]*dims[2]*q0 + dims[2]*q1 + q2];
                            else
                                patch1 = _patch_3d(q0, q1, q2, dims[0], dims[1], dims[2]);

                            diff = patch1 - patch0;

                            diff_sum = 0;
                            for (size_t i=0; i<psize2; ++i) {
                                val = spat_weight.data()[i] * (diff.data()[i]*diff.data()[i] - var2);
                                if (val > 0)
                                    diff_sum += val;
                            }

                            weight = expf(-diff_sum);

                            weight_sum += weight;
                            new_val += weight*_img(q0, q1, q2);
                        }
                    }
                }
                _result(p0, p1, p2) = new_val/weight_sum;
                if (tid == 0) {
                    _completed_iterations += omp_get_num_threads();
                }
            }
        }
    };
}


void Nlm::_nlm_3d_paginated() {
    auto dims = _img.dimensions();
    _result = Tensorfff(dims[0], dims[1], dims[2]);
    _result.setZero();

    int dim0 = dims.at(0);
    int dim1 = dims.at(1);
    int dim2 = dims.at(2);

    for (auto k=0; k<dim2; ++k) {
        if (abort == true) {
            return;
        }

        Eigen::array<Eigen::Index, 3> offset = {0, 0, k};
        Eigen::array<Eigen::Index, 3> extent = {dim0, dim1, 1};

        Eigen::Tensor<float, 3> slice = _img.slice(offset, extent);
        Nlm temp_nlm(slice);
        temp_nlm.setPrecomputePatches(_precompute_patches);
        if (_threshold.has_value()) {
            temp_nlm.setThreshold(_threshold.value());
        }
        temp_nlm.setH(h());
        temp_nlm.setPatchSize(patchSize());
        temp_nlm.setSearchDist(searchDist());
        temp_nlm.setSigma(sigma());

        temp_nlm.process();

        Tensorfff _temp_result = temp_nlm.result();
        for (auto j=0; j<dim1; j++) {
            for (auto i=0; i<dim0; i++) {
                _result(i, j, k) = _temp_result(i, j, 0);
            }
        }
        _completed_iterations++;
    }
}


void Nlm::process() {
    auto start = std::chrono::steady_clock::now();
    abort = false;
    auto dims = _img.dimensions();
    _completed_iterations = 0;
    _progress = 0;

    std::cout << "Patch size: " << _patch_size << std::endl;
    std::cout << "Search distance: " << _search_dist << std::endl;
    std::cout << "Threshold: " << (_threshold.has_value() ? std::to_string(_threshold.value()) : "None") << std::endl;
    std::cout << "h: " << _h << std::endl;
    std::cout << "sigma: " << _sigma << std::endl;

    _dim0_loop.resize(dims[0]);
    for (auto i=0; i<dims[0]; ++i) {_dim0_loop[i] = i;}

    if (precomputePatches() == true) {
        // Do not build the cache for 3D paginated images
        if (not (dims[2] > 1 and _paginate)) {
            _rebuild_patch_cache();
        }
    }

    // 2D
    if (dims[2] == 1) {
        _nlm_2d();
    }
    // 3D
    else {
        if (_paginate) {
            _nlm_3d_paginated();
        }
        else {
            _nlm_3d();
        }
    }

    _patch_cache_2d->clear();
    _patch_cache_3d->clear();

    auto end = std::chrono::steady_clock::now();

    _hours = duration_cast<hours>(end - start).count();
    _mins = duration_cast<minutes>(end - start).count() % 60;
    _secs = (duration_cast<seconds>(end - start).count() % 60) + (duration_cast<milliseconds>(end - start).count() % 1000)/1000.0;

    std::string final_status = abort ? "aborted" :  "finished";
    std::cout << "NLM filter " << final_status << " after "
              << elapsedHours() << " hours "
              << elapsedMins() << " min "
              << elapsedSecs() << " sec" << std::endl;
}

unsigned int Nlm::precomputedPatchMemoryRequired() {
    return patchCacheMemoryRequired(_img, patchSize(), paginate());
}


double patchCacheMemoryRequired(Tensorfff const &array, int patch_size, bool paginated) {
    auto dims = array.dimensions();

    if (not array.size()) {
        return 0;
    }

    unsigned int ndims = 0;
    if (dims[2] == 1) {
        ndims = 2;
    }
    else {
        ndims = 3;
    }

    double memory = 4 * pow(patch_size, ndims) * dims[0] * dims[1] * dims[2]/pow(1024, 3);

    if (paginated and ndims == 3) {
        memory /= dims[2];
    }

    return memory;
}
