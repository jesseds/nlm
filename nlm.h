/************************************************************************
** This file is part of NLM Denoiser, a program for applying NLM filters
** on 2D/3D images.
**
** Copyright (C) 2022 Jesse Smith
**
** NLM Denoiser comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of version 3 of the GNU Lesser General Public
** License as published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with this program; if not, write to the Free Software Foundation,
** Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <unsupported/Eigen/CXX11/Tensor>
#include <optional>
#include <memory>
#include <variant>
#include <chrono>
#include <inout.h>

typedef Eigen::Tensor<float, 3> Tensorfff;

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::hours;
using std::chrono::minutes;
using std::chrono::seconds;

// Determine if two numbers are close
bool isClose(double val1, double val2, double dx = 1e-6);

// Estimate the extra memory required for patch caching
double patchCacheMemoryRequired(Tensorfff const &array, int patch_size, bool paginated);

// Estimate the noise variance using the median absolute deviation
double mad_estimate_noise_variance(Tensorfff const& img, std::optional<double> threshold = std::nullopt);

class Nlm {

private:
    // NLM parameters
    Tensorfff _img;
    unsigned int _patch_size;
    unsigned int _patch_rad;
    std::vector<int> _patch_offset {};
    unsigned int _search_dist;
    float _sigma;
    float _h;
    std::optional<float> _threshold;
    double _progress = 0;
    std::vector<long long> _dim0_loop {};

    // Options
    bool _precompute_patches;
    bool _paginate;
    size_t _completed_iterations = 0;
    std::unique_ptr<std::vector<Tensorfff>> _patch_cache_3d;
    std::unique_ptr<std::vector<Eigen::ArrayXXf>> _patch_cache_2d;

//    void _iterationFinished();
    std::stringstream _iteration_ss;
    void _rebuild_patch_cache();
    void _setup();
    Eigen::ArrayXXf _patch_2d(int p0, int p1, int dim0, int dim1) const;
    Tensorfff _patch_3d(int p0, int p1, int p3, int dim0, int dim1, int dim2) const;
    void _nlm_2d();
    void _nlm_3d();
    void _nlm_3d_paginated();
    int _hours = 0;
    int _mins = 0;
    double _secs = 0;

public:
    explicit Nlm(Tensorfff const& img);
    explicit Nlm(std::string const& path);

    Tensorfff _result;
    bool abort = false;
    inout::TiffScalar tif_vtk_type = inout::TiffScalar::FLOAT;

    // Set the image to perform NLM on
    void setImage(Tensorfff const& val) {_img = val;}
    auto image() const {return _img;}

    // Set the patch size (MxM) or (MxMxM)
    void setPatchSize(unsigned int val);
    auto patchSize() const {return _patch_size;}

    // Whether to precompute and cache all patches in the image. This provides 2-3x speed up
    // but can consume significant amounts of memory for 3D images
    void setPrecomputePatches(bool val) {_precompute_patches = val;};
    auto precomputePatches() const {return _precompute_patches;}

    unsigned int precomputedPatchMemoryRequired();

    // Amount of smoothing parameter
    void setH(float);
    auto h() const {return _h;}

    // Paginate 3D images
    void setPaginate(bool val);
    auto paginate() const {return _paginate;}

    // Smooth only pixels larger than a threshold
    void setThreshold(float val) {_threshold = val;};
    auto threshold() const {return _threshold;}

    // Size of the search neighborhood
    void setSearchDist(unsigned int val);
    auto searchDist() const {return _search_dist;}

    // Standard deviation of noise estimate
    void setSigma(double val);
    auto sigma() const {return _sigma;}

    // Convenience functions for accessing other parameters
    auto patchDist() const {return _patch_rad;}
    auto searchSize() const {return (_search_dist*2) - 1;}

    // NLM result
    auto result() const {return _result;}

    // Elapsed time for NLM to complete
    auto elapsedHours() const {return _hours;}
    auto elapsedMins() const {return _mins;}
    auto elapsedSecs() const {return _secs;}

    double progress() const;// {return _progress;}

    // Perform the NLM computation, result can be accessed through result()
    void process();

};
