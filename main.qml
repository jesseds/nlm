import QtQuick
import QtQml
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Material
import QtCharts
import NlmInterface

import "qml"

ApplicationWindow {
    id: root
    visible: true
    title: "NLM Denoiser"
    minimumHeight: 850
    minimumWidth: 1680

    property int outerMargin: 20
    property int outerTopMargin: 8

    Connections {
        target: NlmInterface
        function onShowMessage(title, text) {
            messageDialog.title = title
            messageDialog.text = text
            messageDialog.open()
        }
    }

    Dialog {
        id: messageDialog
        standardButtons: DialogButtonBox.Ok
        parent: Overlay.overlay
        anchors.centerIn: parent
        modal: true
        Material.roundedScale: Material.LargeScale

        property alias text:  msgText.text

        Label {
            id: msgText
            text: "The current processed image is unsaved, do you wish to exist anyway?"
        }
    }

    Dialog {
        id: closingDialog
        title: "Exiting"
        standardButtons: DialogButtonBox.Discard | DialogButtonBox.Cancel
        parent: Overlay.overlay
        anchors.centerIn: parent
        modal: true
        Material.roundedScale: Material.LargeScale

        Label {
            text: "Current data is unsaved, do you wish to discard and exit?"
        }

        onDiscarded: {
            Qt.quit()
        }
    }

    onClosing: (close) => {
        close.accepted = false
        if (NlmInterface.isUnsaved() === true) {
            closingDialog.open()
        }
        else {
            close.accepted = true
        }
    }

    Dialog {
        id: cleanUpDialog
        title: "Cleaning up ..."
        parent: Overlay.overlay
        width: 600
        dim: false
        anchors.centerIn: parent
        modal: true
        closePolicy: Dialog.NoAutoClose
        Material.roundedScale: Material.LargeScale

        ProgressBar {
            indeterminate: true
            anchors.fill: parent
        }

        Connections {
            target: NlmInterface
            function onNlmWorkerObjDestroyed() {
                cleanUpDialog.visible = false
            }
        }
    }

    Dialog {
        id: progressDialog
        title: ""
        parent: Overlay.overlay
        width: 600
        dim: false
        anchors.centerIn: parent
        modal: true
        closePolicy: Dialog.NoAutoClose
        standardButtons: DialogButtonBox.Cancel
        property date startDateTime: new Date()
        Material.roundedScale: Material.LargeScale

        onRejected: {
            NlmInterface.processCancel()
            cleanUpDialog.open()
        }

        ColumnLayout {
            anchors.fill: parent
            ProgressBar {
                id: progressBar
                Layout.fillWidth: true
                from: 0
                to: 100
            }

            RowLayout {
                Layout.fillWidth: true
                Text {
                    id: currElapsedTime
                    opacity: 0.5
                }

                Item {
                    Layout.fillWidth: true
                }

                Text {
                    Layout.alignment: Qt.AlignRight
                    id: estimatedTime
                    opacity: 0.5
                }
            }
        }

        Connections {
            target: NlmInterface
            function onProcessFinished(valid) {
                progressDialog.visible = false
                progressBar.value = 0
            }

            function onProcessStarted() {
                if (NlmInterface.precomputePatches === true) {
                    progressDialog.title = "Building patch cache..."
                }
                progressDialog.startDateTime = new Date()
                progressDialog.visible = true
            }

            function onProcessProgressChanged(prog) {
                progressBar.value = prog*100
                if (progressBar.value === 0 && NlmInterface.precomputePatches === true) {
                    return
                }
                var diff = (new Date() - progressDialog.startDateTime)/1000 // seconds
                var hours = Math.floor(diff / 3600)
                var mins = Math.floor(diff % 3600 / 60)
                var sec = Math.floor(diff % 3600 % 60)

                currElapsedTime.text = "Elapsed time: " + hours + "hr " + mins + "min " + sec + "s"
                var est = diff/prog * (1-prog)
                var est_hours = Math.floor(est / 3600)
                var est_mins = Math.floor(est % 3600 / 60)
                var est_sec = Math.floor(est % 3600 % 60)

                estimatedTime.text = "Remaining time: " + est_hours + "hr " + est_mins + "min " + est_sec + "s"

                progressDialog.title = "Processing NLM filter (" + progressBar.value.toFixed(2) + "%)"

                if (progressBar.value >= 100 && NlmInterface.precomputePatches === true) {
                    progressDialog.visible = false
                    progressBar.value = 0
                    cleanUpDialog.open()

                }
            }
        }
    }

    Dialog {
        id: loadingDialog
        title: "Loading tiff"
        parent: Overlay.overlay
        width: 600
        dim: false
        anchors.centerIn: parent
        modal: true
        closePolicy: Dialog.NoAutoClose
        Material.roundedScale: Material.LargeScale


        ProgressBar {
            indeterminate: true
            anchors.fill: parent
        }

        Connections {
            target: NlmInterface
            function onTiffLoadingStarted() {
                loadingDialog.open()
            }
            function onTiffLoadingEnded(valid) {
                loadingDialog.close()
            }
        }
    }

    Dialog {
        id: workingDialog
        title: "Working..."
        parent: Overlay.overlay
        width: 600
        dim: false
        anchors.centerIn: parent
        modal: true
        closePolicy: Dialog.NoAutoClose
        Material.roundedScale: Material.LargeScale


        ProgressBar {
            indeterminate: true
            anchors.fill: parent
        }

        Connections {
            target: NlmInterface
            function onShowWorkingProgress() {
                workingDialog.open()
            }
            function onEndWorkingProgress() {
                workingDialog.close()
            }
        }
    }

    OptionsPane {
        id: options
        width: 370
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
    }

    Item {
        id: imageViews
        anchors.left: options.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: 20
        anchors.bottomMargin: 36

        ImageView {
            id: unfilteredImageView
            parent: imageViews
            anchors.left: parent.left
            width: parent.width/2 - 20
            height: parent.height
            title: "Source"
            imageProvider: "image://filterimageprovider/unfiltered/"

            Connections {
                target: NlmInterface
                function onUnfilteredImageChanged() {
                    unfilteredImageView.updateImage()
                }
            }
        }

        ImageView {
            id: filteredImageView
            parent: imageViews
            anchors.right: parent.right
            width: parent.width/2 - 20
            title: "NLM Filtered"
            height: parent.height

            imageProvider: "image://filterimageprovider/filtered/"

            Connections {
                target: NlmInterface
                function onFilteredImageChanged() {
                    filteredImageView.updateImage()
                }

                function onTiffLoadingEnded(valid) {
                    if (valid === true) {
                        filteredImageView.toDefaultImage()
                    }
                }
            }
        }
    }

    Label {
        id: elapsedTimeText
        font.pointSize: 12
        anchors.left: options.right
        anchors.bottom: options.bottom
        anchors.margins: 6
        opacity: 0.4

        Connections {
            target: NlmInterface
            function onNlmWorkerObjDestroyed() {
                var hr = Math.round(NlmInterface.elapsedHours)
                var mins = Math.round(NlmInterface.elapsedMins)
                var secs = NlmInterface.elapsedSecs
                elapsedTimeText.text = "Last completed time: " + hr.toString() + " hours " + mins.toString() + " mins " + secs.toFixed(3).toString() + " secs"
            }
        }
    }
}
