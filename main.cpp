/************************************************************************
** This file is part of NLM Denoiser, a program for applying NLM filters
** on 2D/3D images.
**
** Copyright (C) 2022 Jesse Smith
**
** NLM Denoiser comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of version 3 of the GNU Lesser General Public
** License as published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with this program; if not, write to the Free Software Foundation,
** Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <QQmlApplicationEngine>
#include <QApplication>
#include <QQuickStyle>
#include <QQmlContext>
#include <interface.h>
#include <imageprovider.h>
#include <QIcon>
#include <iostream>
#include <updatechecker.h>


int main(int argc, char *argv[]) {
    std::cout << VERSION << std::endl;
    QCoreApplication::setOrganizationName("Jesse Smith");
    QCoreApplication::setOrganizationDomain("Jesse Smith");

    QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);

    QApplication app(argc, argv);

    Interface intf {};
    ImageProvider* img_provider = new ImageProvider(&intf);
    app.setWindowIcon(QIcon(":/resources/icon.png"));

    QQmlApplicationEngine engine;

    engine.addImageProvider(QLatin1String("filterimageprovider"), img_provider);
    const QUrl url("qrc:/main.qml");

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
    if (!obj && url == objUrl)
        QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    qmlRegisterSingletonInstance<Interface>("NlmInterface", 1, 0, "NlmInterface", &intf);

    QQmlContext* context = engine.rootContext();
    context->setContextProperty("VERSION", VERSION);
    engine.load(url);

    // Check for updates
    UpdateChecker* updater = new UpdateChecker();
    updater->show_no_update_message = false;
    updater->run();

    return app.exec();
}
