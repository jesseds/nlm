![](https://gitlab.com/jesseds/nlm_meta/-/raw/main/nlm.png)

# Non-Local Means Denoiser
NLM Denoiser is a program for applying 2D and 3D Non-Local Means denoising filters to grayscale 2D/3D tif files.

# Downloads
Releases including windows binaries are located here: https://gitlab.com/jesseds/nlm/-/releases
