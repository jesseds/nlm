/************************************************************************
** This file is part of NLM Denoiser, a program for applying NLM filters
** on 2D/3D images.
**
** Copyright (C) 2022 Jesse Smith
**
** NLM Denoiser comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of version 3 of the GNU Lesser General Public
** License as published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with this program; if not, write to the Free Software Foundation,
** Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QObject>
#include <unsupported/Eigen/CXX11/Tensor>
#include <optional>
#include <inout.h>

class TiffReaderWorker : public QObject {
    Q_OBJECT

private:
    QString _path = "";

public:
    explicit TiffReaderWorker(QString const& path);

    std::optional<Eigen::Tensor<float, 3>> result;
    inout::TiffScalar tif_vtk_type = inout::TiffScalar::FLOAT;
    std::string err_msg = "";

public slots:
    void start();

signals:
    void finished();
};

