/************************************************************************
** This file is part of NLM Denoiser, a program for applying NLM filters
** on 2D/3D images.
**
** Copyright (C) 2022 Jesse Smith
**
** NLM Denoiser comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of version 3 of the GNU Lesser General Public
** License as published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with this program; if not, write to the Free Software Foundation,
** Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#define cimg_use_tiff
#include <inout.h>
#include <vector>
#include <Eigen/Dense>
#include <ciso646>
#include <CImg.h>
#include <tiffio.h>
#include <tiff.h>

typedef Eigen::Tensor<float, 3> Tensorfff;
using cimg_library::CImg;


std::pair<Tensorfff, inout::TiffScalar> inout::read_tiff(std::string path) {
    CImg<float> img(path.data());
    std::cout << "Reading file " << path << std::endl;

    int width = img.width();
    int height = img.height();
    int depth = img.depth();

    std::array<int, 3> dims {width, height, depth};

    std::cout << "Dimensions: " << width << " x " << height << " x " << depth << std::endl;

    bool unsupported_dims = false;
    if (dims[0] == 1) {
        unsupported_dims = true;
    }
    else if (dims[1] == 1 and dims[2] == 1) {
        unsupported_dims = true;
    }

    if (unsupported_dims == true) {
        throw std::runtime_error("Only 2D and 3D images are supported");
    }

    Tensorfff retn(width, height, depth);

    for (long k=0; k<dims[2]; ++k) {
        for (long j=0; j<dims[1]; ++j) {
            for (long i=0; i<dims[0]; ++i) {
                retn(i, j, k) = img.atXYZ(i, j, k);
            }
        }
    }

    // Get bit precision of file
    int bits_sample = 0;
    TIFF* tiff = TIFFOpen(path.data(), "r");
    if (tiff) {
        TIFFGetField(tiff, TIFFTAG_BITSPERSAMPLE, &bits_sample);
        std::cout << "Bits/sample: " << bits_sample << std::endl;
    }
    else {
        throw std::runtime_error("Could not determine bits per sample of tiff file");
    }

    TIFFClose(tiff);

    TiffScalar scalar_type;
    switch (bits_sample) {
    case 8:
        scalar_type = inout::TiffScalar::CHAR;
        break;
    case 16:
        scalar_type = inout::TiffScalar::SHORT;
        break;
    case 32:
        scalar_type = inout::TiffScalar::FLOAT;
        break;
    default:
        throw std::runtime_error("Unknown btis/sample");
    }

    return {retn, scalar_type};
}

void inout::write_tiff(std::string path, Tensorfff &data, inout::TiffScalar dtype) {
    std::cout << "Writing file to " << path << std::endl;
    std::cout << "With bit precision " << static_cast<int>(dtype) << std::endl;
    auto dims = data.dimensions();
    unsigned int dim0 = dims[0];
    unsigned int dim1 = dims[1];
    unsigned int dim2 = dims[2];

    CImg<float> img {dim0, dim1, dim2};
    for (long k=0; k<dims[2]; ++k) {
        for (long j=0; j<dims[1]; ++j) {
            for (long i=0; i<dims[0]; ++i) {
                img(i, j, k) = data(i, j, k);
            }
        }
    }

    switch (dtype) {
    case inout::TiffScalar::FLOAT: {
        img.save_tiff(path.data());
        break;
    }
    case inout::TiffScalar::SHORT: {
        auto temp = static_cast<CImg<uint16_t>>(img);
        temp.save_tiff(path.data());
        break;
    }
    case inout::TiffScalar::CHAR: {
        auto temp = static_cast<CImg<uint8_t>>(img);
        temp.save_tiff(path.data());
        break;
    }
    default:
        break;
        throw std::runtime_error("Unknown tiff scalar type");
    }
}
