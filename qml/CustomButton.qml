import QtQuick
//import QtGraphicalEffects 1.15
import QtQuick.Controls
import QtQuick.Controls.Material

Button {
    id: control

//    if (Material.theme == Material.Dark)
    property color backgroundColor: highlighted ? Material.accentColor : Material.theme === Material.Dark ? Qt.lighter(Material.backgroundColor, 1.4) : Qt.darker(Material.backgroundColor, 1.1)
    property color backgroundHoverColor: highlighted ? Qt.lighter(Material.accentColor, 1.1) : Material.theme === Material.Dark ? Qt.lighter(Material.backgroundColor, 1.6) : Qt.darker(Material.backgroundColor, 1.12)
    property color backgroundPressedColor: highlighted ? Qt.lighter(Material.accentColor, 1.2) :  Material.theme === Material.Dark ? Qt.lighter(Material.backgroundColor, 1.8) : Qt.darker(Material.backgroundColor, 1.15)

    signal clicked()
    property alias implicitWidth: rect.implicitWidth

    onClicked: focus = true

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: parent.clicked()
        onEntered: {
            if (control.enabled)
                rect.color = pressed ? backgroundPressedColor : backgroundHoverColor
        }
        onExited: rect.color = pressed ? backgroundPressedColor : backgroundColor
        onPressed: rect.color = backgroundPressedColor
        onReleased: rect.color = backgroundColor
    }

    background: Rectangle {
        id: rect
        radius: 3
        color: highlighted ? Material.accentColor : backgroundColor
//        border.color: highlighted ? Material.accentColor : Qt.lighter(Material.backgroundColor, 1.7)
//        border.width: 1
        implicitWidth: 75

        // Drop shadow
//        layer.enabled: enabled
//        layer.effect: DropShadow {
//            radius: 10
//            samples: radius*2
//            spread: 0
//            color: Qt.darker(Material.background, 1.4)
//        }
    }
}

