import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Material
import QtCharts
import QtQuick.Dialogs
import Qt.labs.platform
import NlmInterface

Rectangle {
    color: Material.theme === Material.Dark ? Qt.lighter(Material.backgroundColor, 1.2) : Qt.darker(Material.backgroundColor, 1.04)
    border.color: Material.theme === Material.Dark ? Qt.lighter(Material.backgroundColor, 1.7) : Qt.darker(Material.backgroundColor, 1.125)
    border.width: 1
    anchors.leftMargin: -1
    anchors.topMargin: -1
    anchors.bottomMargin: -1

    AboutDialog {
        id: aboutDialog
        visible: false
    }

    FileDialog {
        id: readTiffDialog
        title: "Open tif image"
//        folder: StandardPaths.HomeLocation
        fileMode: FileDialog.OpenFile
        nameFilters: ["Tagged Image Format (*.tif *.tiff)"]
        modality: Qt.ApplicationModal
        onAccepted: {
            NlmInterface.tiff = file
        }
    }

    FileDialog {
        id: saveTiffDialog
        title: "Save to tif"
        //        folder: readTiffDialog.folder
        fileMode: FileDialog.SaveFile
        nameFilters: ["Tagged Image Format (*.tif)"]
        modality: Qt.ApplicationModal
        onAccepted: {
            if (file) {
                NlmInterface.saveTiff(file)
            }
        }
    }

    FileDialog {
        id: autoSaveTiffDialog
        title: "Save to tif"
        //        folder: readTiffDialog.folder
        fileMode: FileDialog.SaveFile
        nameFilters: ["Tagged Image Format (*.tif)"]
        modality: Qt.ApplicationModal
        onAccepted: {
            if (file) {
                NlmInterface.autosaveTiffPath = file
            }
        }
    }

    ColumnLayout {
        Layout.alignment: Qt.AlignTop
        anchors.fill: parent
        anchors.margins: outerMargin
        anchors.topMargin: outerTopMargin
        id: optionsArea

        RowLayout {
            Layout.alignment: Qt.AlignBottom
            Layout.rightMargin: -13
            Layout.bottomMargin: -5

            Label {
                text: "Options"
                font.pointSize: 16
                color: Material.theme === Material.Dark ? Qt.lighter(Material.accentColor) : Material.accentColor
                Layout.fillWidth: true
            }
            ToolButton {
                icon.source: "qrc:/resources/about.png"
                icon.color: "transparent"
                Layout.alignment: Qt.AlignRight
                hoverEnabled: true
                ToolTip.text: "About"
                ToolTip.delay: 500
                ToolTip.visible: hovered

                onClicked: aboutDialog.visible = true
            }
        }

        GridLayout {
            id: optionsForm
            columns: 2
            rowSpacing: -8
            Layout.alignment: Qt.AlignTop

            Label {text: "Source:"}
            CustomButton {
                readonly property string defaultText: "OPEN TIF"
                id: readTiffBtn
                text: defaultText
                Layout.fillWidth: true
                font.capitalization: Font.MixedCase
                hoverEnabled: true

                onClicked: {
                    readTiffDialog.open()
                }

                ToolTip.text: readTiffBtn.text === defaultText ? "Load a new tif file" : readTiffBtn.text
                ToolTip.delay: 500
                ToolTip.visible: hovered

                Connections {
                    target: NlmInterface
                    function onTiffLoadingEnded(valid) {
                        if (valid === true) {
                            readTiffBtn.text = NlmInterface.tifName()
                        }
                    }
                }
            }

            Label {
                text: "View slice:"
            }
            RowLayout {
                Slider {
                    id: sliceSlider
                    from: 1
                    to: NlmInterface.nPages
                    value: NlmInterface.currPage
                    Layout.fillWidth: true
                    enabled: NlmInterface.nPages > 1
                    stepSize: 1
                    onValueChanged: {
                        NlmInterface.currPage = value
                    }
                }
                Label {
                    text: sliceSlider.value

                    Layout.fillWidth: true
                    Layout.minimumWidth: 30
                }
            }

            // Patch size
            Label {text: "Patch size (px):"}
            SpinBox {
                id: patchSizeSpin
                from: 3
                to: 49
                stepSize: 2
                value: NlmInterface.patchSize
                editable: true
                hoverEnabled: true
                ToolTip.text: "Size of patches (MxM or MxMxM) used to compare similarity of pixels"
                ToolTip.visible: hovered
                ToolTip.delay: 500
                Layout.fillWidth: true
                background: Rectangle{color: "black"; opacity: 0.02; height:32; width: 190; anchors.verticalCenter: parent.verticalCenter; anchors.horizontalCenter: parent.horizontalCenter}

                onValueChanged: NlmInterface.patchSize = value
            }

            Label {text: "Search distance (px):"}
            SpinBox {
                id: searchDistSpin
                from: 1
                to: 999
                value: NlmInterface.searchDist
                editable: true
                hoverEnabled: true
                ToolTip.text: "Distance to search for similar pixels/patches"
                ToolTip.visible: hovered
                ToolTip.delay: 500
                Layout.fillWidth: true
                onValueChanged: NlmInterface.searchDist = value
                background: Rectangle{color: "black"; opacity: 0.02; height:32; width: 190; anchors.verticalCenter: parent.verticalCenter; anchors.horizontalCenter: parent.horizontalCenter}
            }

            Label {text: "Threshold (opt):"}
            Row {
                padding: 0
                Layout.fillWidth: true
                TextField {
                    id: thresholdInput
                    //                placeholderText: "(optional)"
                    text: NlmInterface.thresholdStr
                    validator: RegularExpressionValidator {regularExpression: /(-?\d+\.?\d*)|()/}
                    selectByMouse: true
                    hoverEnabled: true
                    width:parent.width
                    ToolTip.text: "Only pixels with scalars larger than this will be denoised"
                    ToolTip.visible: hovered
                    ToolTip.delay: 500
                    implicitHeight: 40
                    background: Rectangle{color: "transparent"}
                    Rectangle {
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 4
                        width: parent.width
                        height: 1
                        color: "black"
                        opacity: 0.2
                    }

                    onEditingFinished: NlmInterface.thresholdStr = text
                }
            }

            Label {text: "h:"}
            Row {
                Layout.fillWidth: true
                TextField {
                    id: hInput
                    text: NlmInterface.h
                    validator: DoubleValidator {bottom: 0; }
                    selectByMouse: true
                    hoverEnabled: true
                    width: parent.width
                    ToolTip.text: "Controls permisiveness in determining patch similarity (i.e. level of smoothing)"
                    ToolTip.visible: hovered
                    ToolTip.delay: 500

                    onEditingFinished: {
                        NlmInterface.h = text
                    }

                    implicitHeight: 40
                    background: Rectangle{color: "transparent"}
                    Rectangle {
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 4
                        //                    anchors.
                        width: parent.width
                        height: 1
                        color: "black"
                        opacity: 0.2
                    }
                }
            }

            Label {text: "Sigma:"}

            RowLayout {
                TextField {
                    id: sigmaInput
                    text: NlmInterface.sigma
                    validator: DoubleValidator {bottom: 0}
                    selectByMouse: true
                    Layout.fillWidth: true
                    hoverEnabled: true
                    ToolTip.text: "Estimated (gaussian) standard deviation of noise in image"
                    ToolTip.visible: hovered
                    ToolTip.delay: 500
                    onEditingFinished: NlmInterface.sigma = text
                    implicitHeight: 40
                    background: Rectangle{color: "transparent"}
                    Rectangle {
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 4
                        width: parent.width
                        height: 1
                        color: "black"
                        opacity: 0.2
                    }
                }

                ToolButton {
                    id: estimateSigma
                    icon.source: "qrc:/resources/view-refresh.svg"
                    ToolTip.text: "Estimate the noise variance (median absolute deviation)"
                    ToolTip.visible: hovered
                    ToolTip.delay: 500
                    onClicked: NlmInterface.estimateNoiseVariance()
                }

            }

            Label {text: "Paginate 3D images:"}
            CheckBox {
                id: paginationCheckBox
                checked: NlmInterface.paginate
                hoverEnabled: true
                ToolTip.text: "Compute 3D NLM filters in 2D per-slice (faster, but less denoising)"
                ToolTip.visible: hovered
                ToolTip.delay: 500
                onCheckedChanged: {
                    NlmInterface.paginate = checked
                    patchCacheMemoryRequiredText.updateText()
                }
                }

            Label {text: "Precompute patches:"}
            RowLayout {
                CheckBox {
                    id: precomputeCheckBox
                    checked: NlmInterface.precomputePatches
                    hoverEnabled: true
                    ToolTip.text: "Increases speed by 2-3x but may require significant memory capacity for 3D images"
                    ToolTip.visible: hovered
                    ToolTip.delay: 500
                    onCheckedChanged: {
                        NlmInterface.precomputePatches = checked
                        checked ? patchCacheMemoryRequiredText.visible = true : patchCacheMemoryRequiredText.visible = false
                    }
                }
                Label {
                    id: patchCacheMemoryRequiredText
                    opacity: 0.3
                    visible: false

                    function updateText() {
                        patchCacheMemoryRequiredText.text = "Extra <b>" + NlmInterface.patchCacheMemoryRequiredStr() + " Gb</b> requried"
                    }

                    Connections {
                        target: NlmInterface
                        function onTiffLoadingEnded(valid) {
                            if (valid) {
                                patchCacheMemoryRequiredText.updateText()
                            }
                            else {
                                patchCacheMemoryRequiredText.text = ""
                            }
                        }
                        function onPatchSizeChanged() {
                            if (patchCacheMemoryRequiredText.text != "")
                                patchCacheMemoryRequiredText.updateText()
                        }
                    }
            }
            }
            Label {text: "Autosave:" }
            RowLayout {
                CheckBox {
                    id: autoSaveEnabled
                    checked: NlmInterface.autosaveEnabled
                    hoverEnabled: true
                    onCheckedChanged: NlmInterface.autosaveEnabled = checked

                    ToolTip.text: "Automatically save tiff file when completed"
                    ToolTip.delay: 500
                    ToolTip.visible: hovered
                }

                CustomButton {
                    enabled: autoSaveEnabled.checked
                    readonly property string defaultText: "NO PATH"
                    id: autoSaveBtn
                    text: defaultText
                    Layout.fillWidth: true
                    font.capitalization: Font.MixedCase
                    hoverEnabled: true

                    onClicked: {
                        autoSaveTiffDialog.open()
                    }

                    ToolTip.text: autoSaveBtn.text === defaultText ? "Save location" : autoSaveBtn.text
                    ToolTip.delay: 500
                    ToolTip.visible: hovered

                    Connections {
                        target: autoSaveTiffDialog
                        function onAccepted() {
                        autoSaveBtn.text = NlmInterface.autosaveTifName()
                    }
                    }
                }
            }
        }
        RowLayout {
            Layout.alignment: Qt.AlignTop
            CustomButton {
                text: "Save tif"
                Layout.fillWidth: true
                hoverEnabled: true
                enabled: NlmInterface.resultAvailable
                onClicked: {
                    saveTiffDialog.open()
                }
            }
            CustomButton {
                id: executeBtn
                text: "Process"
                highlighted: true
                Layout.fillWidth: true
                hoverEnabled: true
                focusPolicy: Qt.StrongFocus
                onClicked: {
                    NlmInterface.processStart()
                }
            }
        }

        Rectangle {
            color: "black"
            opacity: 0.1
            Layout.fillWidth: true
            height: 1
        }

        Item {height: 5}
        HistogramChart {
            id: unfilteredHistogram
            Layout.fillWidth: true
            Layout.minimumHeight: 170
            titleText: "Source histogram"

            Connections {
                target: NlmInterface
                function onUnfilteredHistValChanged(point) {
                    unfilteredHistogram.registerPoint(point.x, point.y)
                }

                function onUnfilteredHistogramComputed() {
                    unfilteredHistogram.updateHistAxisMinMax()
                    unfilteredHistogram.autoXNumbers()
                }

                function onTiffLoadingEnded(valid) {
                    if (valid === true) {
                        unfilteredHistogram.clearHist()
                    }
                }
            }
        }
        HistogramChart {
            id: filteredHistogram
            Layout.fillWidth: true
            Layout.minimumHeight: 170
            titleText: "Filtered histogram"

            Connections {
                target: NlmInterface

                function onFilteredHistValChanged(point) {
                    filteredHistogram.registerPoint(point.x, point.y)
                }

                function onProcessFinished(valid) {
                    if (valid === true) {
                        filteredHistogram.clearHist()
                    }
                }

                function onFilteredHistogramComputed() {
                    filteredHistogram.updateHistAxisMinMax()

                    // Make sure the filterested histogrma has the same x bounds as the unfiltered
                    filteredHistogram.xMin = unfilteredHistogram.xMin
                    filteredHistogram.xMax = unfilteredHistogram.xMax
                    filteredHistogram.autoXNumbers()
                }

                function onTiffLoadingEnded(valid) {
                    if (valid === true) {
                        filteredHistogram.clearHist()
                    }
                }
            }
        }
        Item {Layout.fillHeight: true}
    }
}
