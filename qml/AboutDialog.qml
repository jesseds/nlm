import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Material


Dialog {
    title: "About NLM Denoiser " + VERSION
    standardButtons: DialogButtonBox.Close
    parent: Overlay.overlay
    width: 400
    dim: false
    anchors.centerIn: parent
    modal: true

    Material.roundedScale: Material.LargeScale

    TextArea {
        selectByMouse: true
        horizontalAlignment: Qt.AlignHCenter
        anchors.fill: parent
        text: "Copyright 2021-2022 Jesse Smith
https://gitlab.com/jesseds/nlm\n

NLM Denoiser is a program for denoising 2D/3D TIF images using the non-local means algorithm.\n

NLM Denoiser comes with ABSOLUTELY NO WARRANTY. NLM Denoiser is Free Software and you are entitled to distribute it under the terms of the Lesser GNU Public License (GPL). See the file LICENSE for details."
        readOnly: true
        background: null
        wrapMode: TextArea.WordWrap
    }
}
