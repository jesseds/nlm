import QtQuick.Controls
import QtQuick
import QtQuick.Layouts
import QtQuick.Controls.Material

Item {
    id: root
    required property string imageProvider
    property int index: 0
    property string title: ""

    function updateImage() {
        imageItem.source = imageProvider + root.index
        root.index += 1
    }

    function toDefaultImage() {
        imageItem.source = "qrc:/resources/placeholder.png"
    }

    ColumnLayout {
        anchors.fill: root
        spacing: 10

        Label {
            id: imageLabel
            text: title
            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            color: Material.theme === Material.Dark ? Qt.lighter(Material.accentColor) : Material.accentColor
            font.pointSize: 16
        }

        Image {
            id: imageItem
            cache: false
            Layout.fillWidth: true
            Layout.fillHeight: true
            verticalAlignment: Image.AlignTop
            horizontalAlignment: Image.AlignHCenter
            fillMode: Image.PreserveAspectFit
            source: "qrc:/resources/placeholder.png"
            smooth: false

            layer.enabled: enabled
        }
    }
}
