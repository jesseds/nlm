import QtCharts
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Controls.Material

Item {
    id: root
    property string titleText: ""
    property real xMin: 0
    property real xMax: 1

    Label {
        id: titleTextLbl
        text: titleText
        horizontalAlignment: Qt.AlignHCenter
        opacity: 0.5
        anchors.top: parent.top
        width: parent.width
    }

    function clearHist() {
        histLine.removePoints(0, histLine.count)
        histLine.clear()
        histLineArea.visible = false
        root.xMin = 0
        histAxisY.min = 0
        root.xMax = 1
        histAxisY.max = 1

    }

    function autoXNumbers() {
        histAxisX.applyNiceNumbers()
    }

    function updateHistAxisMinMax() {
        if (histLine.count === 0) {
            histAxisX.min = 0
            histAxisY.min = 0
            histAxisX.max = 1
            histAxisY.max = 1
            return
        }

        var xy0 = histLine.at(0)
        var xmin = xy0.x
        var xmax = xy0.x
        var ymin = xy0.y
        var ymax = xy0.y

        var X = 0
        var Y = 0
        var xy

        for (var i=1; i<histLine.count; ++i) {
            xy = histLine.at(i)
            X = xy.x
            Y = xy.y

            if (X > xmax) {
                xmax = X
            }

            if (X < xmin) {
                xmin = X
            }

            if (Y > ymax) {
                ymax = Y
            }

            if (Y < ymin) {
                ymin = Y
            }
        }

        root.xMin = xmin
        root.xMax = xmax
        histAxisY.min = ymin
        histAxisY.max = ymax*1.05
    }

    function registerPoint(x, y) {
        histLine.append(x, y)
        histLineArea.visible = true
    }

    ChartView {
        id: chart
        antialiasing: true
        backgroundColor: "transparent"
        legend.visible: false
        margins.left: 0
        margins.right: 0
        margins.top: 0
        margins.bottom: 0
        anchors.top: titleTextLbl.bottom
        anchors.bottom: parent.bottom
        width: parent.width

        ValueAxis {
            id: histAxisX
            min: root.xMin
            max: root.xMax
            gridVisible: false
        }
        ValueAxis {
            id: histAxisY
            min: 0
            max: 1
            gridVisible: false
            visible: false
        }

        AreaSeries {
            id: histLineArea
            borderWidth: 1
            axisX: histAxisX
            axisY: histAxisY
            color: Qt.lighter(Material.accentColor, 2.15)
            borderColor: Material.accentColor

            upperSeries: LineSeries {
                id: histLine
            }
        }
    }
}
